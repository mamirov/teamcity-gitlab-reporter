package com.mamirov.gitreporter;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import sun.misc.BASE64Encoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.net.URL;
import java.net.URLConnection;

public class TeamCityXMLParser {
    private static String SERVER_URL = Config.getTeamCityServerUrl();
    private static String BUILDS_XML_URL = "/httpAuth/app/rest/builds/?locator=branch:default:any,project:"+Config.getTeamCityProject();
    private static NamedNodeMap buildData = parseXML(SERVER_URL + BUILDS_XML_URL, "build");

    public static Node getNode(String xmlUrl, String tag, int num) {
        try{
            URL url = new URL(xmlUrl);
            String userPassword = Config.getTeamCityUserPassword();
            String encoding = new BASE64Encoder().encode(userPassword.getBytes());
            URLConnection uc = url.openConnection();
            uc.setRequestProperty("Authorization","Basic " + encoding);
            uc.connect();
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(uc.getInputStream()));
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName(tag);
            return nodeList.item(num);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static NamedNodeMap parseXML(String xmlUrl, String tag){
        return getNode(xmlUrl, tag, 0).getAttributes();
    }

    public static String getBuildAttribute(String attribute){
        return buildData.getNamedItem(attribute).getNodeValue();
    }


//    public static String getIssue(){
//        Node issueTag = parseXML(SERVER_URL +"httpAuth/app/rest/builds/id:"+getBuildId()+"/relatedIssues", "issue").getNamedItem("id");
//        return issueTag.getNodeValue();
//    }

    public static String getStatusBuild(){
        return getBuildAttribute("status");
    }

    public static String getBranchName (){
        return getBuildAttribute("branchName");
    }

    public static String getBuildTypeId(){
        return getBuildAttribute("buildTypeId");
    }

    public static String getBuildHref(){
        return getBuildAttribute("href");
    }

    public static  String getWebUrl(){
        return getBuildAttribute("webUrl");
    }

    public static String getBuildTestsStatus(){
        return getNode(SERVER_URL + getBuildHref(), "statusText", 0).getTextContent();
    }

    public static String getBuildId(){
        return getBuildAttribute("id");
    }
}
