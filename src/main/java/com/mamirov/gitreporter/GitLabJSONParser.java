package com.mamirov.gitreporter;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class GitLabJSONParser {

    public static String PROJECT_ID = Config.getGitProjectId();

    public static JsonArray getJsonArray(){
        try {
            int n = 1;
            JsonArray jsonArray = new JsonArray();
            while (n>0){
                URL url = new URL(Config.getGitLabUrl()+"/api/v3/projects/"+PROJECT_ID+":id/merge_requests?per_page=100&page="+n+"&private_token="+Config.getGitLabPrivateToken());
                URLConnection uc = url.openConnection();
                uc.connect();
                JsonElement jsonElement = new JsonParser().parse(new InputStreamReader(uc.getInputStream()));
                JsonArray gitJsonArray = jsonElement.getAsJsonArray();
                Integer size = gitJsonArray.size();
                if(size<1){
                    return jsonArray;
                }
                for(JsonElement gitJsonElement : gitJsonArray){
                    jsonArray.add(gitJsonElement);
                }
                n++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JsonArray();
    }

    public static String getMergeRequestId(){
        String tcBranchName = TeamCityXMLParser.getBranchName();
        System.out.println("Source branch: "+tcBranchName);
        for(JsonElement jsonElement : getJsonArray()){
            JsonObject jsonObject = jsonElement.getAsJsonObject();
            if(jsonObject.get("source_branch").toString().contains(tcBranchName)){
                if(jsonObject.get("state").toString().contains("opened")){
                    return jsonObject.get("id").toString();
                }
                else{
                    System.out.println("Not find active merge requests");
                }
            }
        }
        return "";
    }

    public static void main (String [] args){
        String mergeRequestId = getMergeRequestId();
        System.out.println("Merge Request ID : "+ mergeRequestId);
        HttpClient httpClient = HttpClientBuilder.create().build();
        try{
            String comment;
            if(TeamCityXMLParser.getStatusBuild().equals("SUCCESS")){
                comment = "![build success]("+Config.getTeamCityServerUrl()+"/img/buildStates/buildSuccessful.png ) ["+TeamCityXMLParser.getBuildTestsStatus()+"]("+TeamCityXMLParser.getWebUrl()+"&tab=testsInfo)";
            }
            else {
                comment = "![build failure]("+Config.getTeamCityServerUrl()+"/img/buildStates/buildFailed.png ) ["+TeamCityXMLParser.getBuildTestsStatus()+"]("+TeamCityXMLParser.getWebUrl()+"&tab=testsInfo) Build Configuration: " + TeamCityXMLParser.getBuildTypeId();
            }
            HttpPost request = new HttpPost(Config.getGitLabUrl()+"/api/v3/projects/"+PROJECT_ID+":id/merge_request/"+mergeRequestId+":merge_request_id/comments?private_token="+Config.getGitLabPrivateToken());
            StringEntity params = new StringEntity("{\n" +
                    "    \"author\":{\n" +
                    "        \"blocked\":false,\n" +
                    "        \"created_at\":\"2012-04-29T08:46:00Z\"\n" +
                    "    },\n" +
                    "    \"note\":\""+comment+"\"\n" +
                    "}");
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            HttpResponse response = httpClient.execute(request);
            System.out.println("Http Response: " + response);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
