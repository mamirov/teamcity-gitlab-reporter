package com.mamirov.gitreporter;


import java.io.FileInputStream;
import java.util.Properties;

public class Config {
    public static Properties props = new Properties();
    static {
        try{
            FileInputStream fis = new FileInputStream("config.properties");
            props.load(fis);
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getGitProjectId(){
        return props.getProperty("gitlab.project.id");
    }

    public static String getGitLabUrl(){
        return props.getProperty("gitlab.server.url");
    }

    public static String getTeamCityProject(){
        return props.getProperty("teamcity.project.name");
    }

    public static String getTeamCityServerUrl(){
        return props.getProperty("teamcity.server.url");
    }

    public static String getTeamCityUserPassword(){
        return props.getProperty("teamcity.basic.user.password");
    }

    public static String getGitLabPrivateToken(){
        return props.getProperty("gitlab.private.token");
    }
}
